#include <vector>
#include "raylib.h"
#include "tea/math.h"

#define RAYGUI_SUPPORT_RICONS
#define RAYGUI_IMPLEMENTATION
#include "ray/raygui.h"                 // Required for GUI controls

int main()
{
    // Initialisation
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;

    InitWindow(screenWidth, screenHeight, "raylib - tea");

    SetTargetFPS(60);

	//--------------------------------------------------------------------------------------
    // Main game loop
    while (!WindowShouldClose())
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(RAYWHITE);

            DrawText("Raylib!", 190, 200, 20, LIGHTGRAY);

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}
